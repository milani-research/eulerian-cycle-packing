module Main where

import Control.Monad
import Data.List
import Output
import qualified Data.Map as M
import qualified HGraph.Directed.AdjacencyMap as AM
import qualified HGraph.Directed as D
import qualified HGraph.Directed.EditDistance.Acyclic.ArcDeletion as FS
import qualified HGraph.Directed.Load as D
import qualified HGraph.Directed.Packing.Cycles.ArcDisjoint as CP
import System.Directory
import System.Environment
import System.Exit
import System.FilePath
import System.IO

main = do
  args <- getArgs
  case args of
    [] -> do
      putStrLn "Please provide an input .gv or a directory."
      exitWith $ ExitFailure 1
    files -> do
      results <- forM files $ \path -> do
        isDir <- doesDirectoryExist path
        files' <- 
          if isDir then
            fmap (map (path </>)) $ listDirectory path
          else
            return [path]
        files'' <- fmap (map fst . filter snd) $ forM files' $ \fl -> do
          exists <- doesFileExist fl
          return (fl, exists)
        forM files'' $ \fl -> withFile fl ReadMode $ \h -> do
          str <- hGetContents h
          case D.loadDot AM.emptyDigraph str of
            Left err -> do
              return $ Left (fl, err)
            Right (d, ltoi, itol, nodeAttr, edgeAttr) ->
              let (cyclePacking, kCycles) = CP.optimal d
                  (hittingSet, kHittingSet) = FS.minimumI d
              in do
                 if (kCycles /= kHittingSet) then do
                   putStrLn $ fl ++ ": " ++ "maximum cycle packing = " ++ show kCycles ++ ", minimum feedback arc set = " ++ show kHittingSet
                   putStrLn $ "Cycle packing:" ++ (intercalate "\n\t" $ map (\vs -> intercalate "->" $ map (itol M.!) vs) cyclePacking) 
                   putStrLn $ "Hitting set:" ++ (intercalate ", " $ map (\(v,u) -> show (itol M.! v, itol M.! u)) hittingSet)
                   return $ Right False
                 else
                   return $ Right True
      good <- forM (concat results) $ \r ->
                case r of
                  Right isGood -> return isGood
                  Left (fl, err) -> do
                    putStrLn $ fl ++ ":\n\t" ++ err
                    return False
      when (all id good) $
        putStrLn "maximum cycle packing = minimum feedback arc set  holds for all input digraphs."

