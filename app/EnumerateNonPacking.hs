module Main where

import qualified HGraph.Directed as D
import qualified HGraph.Directed.Output as D
import qualified HGraph.Directed.Packing.Cycles.ArcDisjoint as CP
import qualified HGraph.Directed.EditDistance.Acyclic.ArcDeletion as FS
import qualified HGraph.Directed.AdjacencyMap as AM
import qualified HGraph.Directed.Generator.Eulerian as Eulerian
import HSParallel.ProducerConsumer
import Control.Concurrent
import Control.Monad
import Data.List
import System.Directory
import System.Environment
import System.IO
import System.FilePath
import Text.Read
import Output

-- import qualified Debug.Trace as D (trace)

data GenState = 
  GenState
  { genVertices           :: Int
  , genSize               :: Int
  , genTotalDigraphs      :: Int
  , genTotalNonPacking    :: Int
  , genBestAdditive       :: (FilePath, Int, Int)
  , genBestMultiplicative :: (FilePath, Int, Int)
  }

main :: IO ()
main = do
  hSetBuffering stdout LineBuffering
  args <- getArgs
  case args of
    [nStr, destination] ->
      let nE = readEither nStr
      in 
      case nE of
        Left err -> do
          putStrLn err
        Right n -> do
          createDirectoryIfMissing True destination
          indexM <- newMVar 1
          resultM <- newEmptyMVar
          stateM <- newMVar $ GenState{ genVertices = 2
                                      , genSize = 0
                                      , genBestAdditive = ("", 0, 0)
                                      , genBestMultiplicative = ("", 1, 1)
                                      , genTotalDigraphs = 0
                                      , genTotalNonPacking = 0
                                      }
          forkIO $ Eulerian.enumerateParallel AM.emptyDigraph 2 n 1 4 [] resultM (Just $ 5 * 60)
          produceConsumeByGeneration
            2
            (do
             res <- takeMVar resultM
             case res of
               Just (d,i) -> return $ Just ((d,i), D.numVertices d)
               Nothing -> do
                putMVar resultM Nothing
                return Nothing
            ) 
            (\(d, _) _ -> do
              check stateM destination d indexM
            )
            (\g -> do
              modifyMVar_ stateM $ \state -> do
                putStrLn $ summary state
                return state{genVertices = (g + 1), genSize = 0}
            )
          -- state <- readMVar stateM
          -- putStrLn $ summary state
          putStrLn $ "Done. Output written to " ++ destination
    _ -> do
      putStrLn "Please provide the maximum number of vertices desired and the output directory."

check stateM destination d countM = do
  updateGenerationVertices stateM d
  let (cyclePacking, kCycles) = CP.optimal d
      (hittingSet, kHittingSet) = FS.minimumI d
  when (kCycles /= kHittingSet) $ do
    i <- modifyMVar countM (\i -> return (i+1, i))
    let destFile = (destination
                      </> (intercalate "_" [ show i
                                           , "n:" ++ show (D.numVertices d)
                                           , "hs:" ++ show kHittingSet
                                           , "cp:" ++ show kCycles
                                           ])
                      <.> "gv")
    writeFile 
      destFile
      (D.toDot d $ markCycles cyclePacking $ markHittingSet hittingSet D.defaultDotStyle)
    modifyMVar_ stateM (\state -> return $ newNonPacking state destFile kHittingSet kCycles)

newNonPacking state fl kHittingSet kCycles = 
  state
  { genTotalNonPacking = genTotalNonPacking state + 1
  , genBestAdditive =
      let old@(bestFl, bestFAS, bestCP) = genBestAdditive state
      in
      if bestFAS - bestCP < kHittingSet - kCycles then
        (fl, kHittingSet, kCycles)
      else
        old
  , genBestMultiplicative =
      let old@(bestFl, bestFAS, bestCP) = genBestAdditive state
      in
      if bestFl == "" then
        (fl, kHittingSet, kCycles)
      else if bestFAS * kCycles < kHittingSet * bestCP then
        (fl, kHittingSet, kCycles)
      else
        old
  }
  
updateGenerationVertices stateM d = do
  modifyMVar_ stateM 
    (\state -> do
        -- if D.numVertices d > genVertices state then do
          -- let n' = (D.numVertices d)
          -- putStrLn $ summary state
        --  return state{genVertices = n', genSize = 1, genTotalDigraphs = genTotalDigraphs state + 1}
        -- else
      return state{genTotalDigraphs = genTotalDigraphs state + 1, genSize = genSize state + 1}
    )

summary state = 
  intercalate "\n"
    [ intercalate " "
        [ "Generated and checked all" 
        , (show $ genSize state)
        , "digraphs with"
        , (show $ genVertices state)
        , "vertices."
        ]
    , intercalate "\n"
        [ "Best gaps found so far:"
        , let (fl, hs, cp) = genBestAdditive state in 
          if fl /= "" then
            intercalate "\n  "
              [ "Additive:"
              , "instance      = " ++ fl
              , "hitting set   = " ++ show hs
              , "cycle packing = " ++ show cp
              , "gap           = " ++ show (hs - cp)
              ]
          else
            ""
        , let (fl, hs, cp) = genBestMultiplicative state in 
          if fl /= "" then
            intercalate "\n  "
              [ "Multiplicative:"
              , "instance      = " ++ fl
              , "hitting set   = " ++ show hs
              , "cycle packing = " ++ show cp
              , "gap           = " ++ show ((toRational hs) / (toRational cp))
              ]
          else
            ""
        , "Digraphs generated:" ++ show (genTotalDigraphs state)
        , "Non-packing digraphs found: " ++ show (genTotalNonPacking state)
        , "######"
        ]
    ]
