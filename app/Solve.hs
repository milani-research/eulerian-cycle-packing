{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified HGraph.Directed as D
import qualified HGraph.Directed.AdjacencyMap as AM
import qualified HGraph.Directed.Load as D
import qualified HGraph.Directed.Packing.Cycles.ArcDisjoint as CP
import qualified HGraph.Directed.EditDistance.Acyclic.ArcDeletion as FS
import qualified HSParallel.ProducerConsumer as Par
import qualified Codec.Archive.Tar as Tar
import qualified Codec.Archive.Tar.Entry as Tar
import qualified Codec.Compression.Lzma as Lzma
import qualified Data.ByteString.Lazy as BS
import qualified Data.Trie.Set as Trie
import qualified Data.Text.Lazy as T
import qualified Data.Text.Lazy.Encoding as T
import qualified Data.Csv as Csv
import qualified Data.Csv.Incremental as CsvI
import Control.Concurrent
import Control.Concurrent.MVar
import Control.Monad
import Data.List
import System.Clock
import System.Directory
import System.Environment
import System.Exit
import System.FilePath
import System.IO
import System.Signal

data Result =
  Result
  { rsName :: String
  , rsVertices :: Int
  , rsArcs :: Int
  , rsFAS :: Int
  , rsFASSeconds :: Int
  , rsCP :: Int
  , rsCPSeconds :: Int
  }

instance Csv.FromNamedRecord Result where
  parseNamedRecord m = do
    name      <- m Csv..: "name"
    nVertices <- m Csv..: "vertices"
    nArcs     <- m Csv..: "arcs"
    fas       <- m Csv..: "feedback arc set"
    fasS      <- m Csv..: "feedback arc set (s)"
    cp        <- m Csv..: "cycle packing"
    cpS       <- m Csv..: "cycle packing (s)"
    return $ Result
      { rsName = name
      , rsVertices = nVertices
      , rsArcs = nArcs
      , rsFAS = fas
      , rsFASSeconds = fasS
      , rsCP = cp
      , rsCPSeconds = cpS
      }

instance Csv.ToNamedRecord Result where
    toNamedRecord result = Csv.namedRecord
        [ "name"                 Csv..= (rsName result)
        , "vertices"             Csv..= (rsVertices result)
        , "arcs"                 Csv..= (rsArcs result)
        , "feedback arc set"     Csv..= (rsFAS result)
        , "feedback arc set (s)" Csv..= (rsFASSeconds result)
        , "cycle packing"        Csv..= (rsCP result)
        , "cycle packing (s)"    Csv..= (rsCPSeconds result)
        ]
instance Csv.ToRecord Result where
    toRecord result = Csv.record 
        [ Csv.toField (rsName result)
        , Csv.toField (rsVertices result)
        , Csv.toField (rsArcs result)
        , Csv.toField (rsFAS result)
        , Csv.toField (rsFASSeconds result)
        , Csv.toField (rsCP result)
        , Csv.toField (rsCPSeconds result)
        ]
instance Csv.DefaultOrdered Result where
    headerOrder _ = Csv.header
      [ "name"
      , "vertices"
      , "arcs"
      , "feedback arc set"
      , "feedback arc set (s)"
      , "cycle packing"
      , "cycle packing (s)"
      ]

main :: IO ()
main = do
  args <- getArgs
  case args of
    csvFl : inputFls -> do
      csvExists <- doesFileExist csvFl
      solved <-
        if csvExists then do
          solvedE <- readResults csvFl
          case solvedE of
            Right solved' -> return solved'
            Left err -> do
              putStrLn $ "Error parsing " ++ csvFl ++ ":\n  "
              putStrLn err
              exitWith $ ExitFailure 2
        else do
          writeFile csvFl "name,vertices,arcs,feedback arc set, feedback arc set (s), cycle packing, cycle packing (s)\n"
          return $ Trie.empty
      csvHandle <- openFile csvFl AppendMode
      csvM <- newMVar csvHandle
      installHandler sigINT
        (\_ -> do
               hPutStrLn stderr $ "Got sigint. Closing csv file."
               h <- takeMVar csvM
               hClose h
               exitWith $ ExitFailure 3
        )
      installHandler sigTERM
        (\_ -> do
               hPutStrLn stderr $ "Got sigterm. Closing csv file."
               h <- takeMVar csvM
               hClose h
               exitWith $ ExitFailure 3
        )
      digraphM <- newEmptyMVar
      _ <- forkIO $ do
                    forM inputFls (digraphsInPath digraphM solved)
                    putStrLn "Input done"
                    putMVar digraphM Nothing
      _ <- Par.produceConsumeExhaustive (takeMVar digraphM) 
                                   (\(d, dname) -> do
                                      result <- solve d dname
                                      withMVar csvM $ \h -> do
                                        hPutStrLn h $ resultToCsv result
                                   )
      hClose csvHandle
    _ -> do
      putStrLn "usage: solve <CSV.file> <digraphs.tar.xz | digraph.gv | directory ...>"
      exitWith $ ExitFailure 1

resultToCsv result = intercalate ","
  [ ('"' : rsName result) ++ "\""
  , show $ rsVertices result
  , show $ rsArcs result
  , show $ rsFAS result
  , show $ rsFASSeconds result
  , show $ rsCP result
  , show $ rsCPSeconds result
  ]

readResults :: FilePath -> IO (Either String (Trie.TSet Char))
readResults csvFl = do
  withFile csvFl ReadMode $ \h -> do
    csv <- BS.hGetContents h
    if BS.null csv then
      return $ Right Trie.empty
    else do
      let trie = do
                (_, rs) <- Csv.decodeByName csv
                let trie = foldr (\r t -> Trie.insert (rsName r) t) Trie.empty rs
                return $ (Trie.count trie) `seq` trie
      case trie of
        Left _ -> return ()
        Right t -> putStrLn $ show $ Trie.count t
      return trie

digraphsInPath digraphM solved path = do
  isDir <- doesDirectoryExist path
  if isDir then do
    digraphsInDirectory digraphM solved path
  else
    case takeExtensions path of
      ".tar.xz" ->
        digraphsInTarXz digraphM solved path
      ".gv" -> do
        when (not $ path `Trie.member` solved) $ 
          digraphDot digraphM path
      _ -> 
        putStrLn $ "Unrecognized file format '" ++ takeExtensions path ++ "' of file " ++ path

digraphsInDirectory digraphM solved dir = do
  paths <- listDirectory dir
  _ <- forM paths $ \p -> digraphsInPath digraphM solved (dir </> p)
  return ()

digraphsInTarXz digraphM solved archive = do
  xzBS <- BS.readFile archive
  let tar = Lzma.decompress xzBS
  digraphsInTar digraphM solved $ Tar.decodeLongNames $ Tar.read tar

digraphsInTar digraphM solved (Tar.Done) = return ()
digraphsInTar digraphM solved (Tar.Next tEntry tRest) = do
  when (not $ (Tar.entryTarPath tEntry) `Trie.member` solved) $
    case Tar.entryContent tEntry of
      Tar.NormalFile bs _ -> 
        let result = do
                     txt <- fmap T.unpack $ 
                        case T.decodeUtf8' bs of
                          Left err -> Left $ show err
                          Right r -> Right r
                     (d, _, _, _, _) <- D.loadDot AM.emptyDigraph txt
                     return d
        in
        case result of
          Left err -> do
            putStrLn $ "Error parsing " ++ (Tar.entryTarPath tEntry) ++ ":\n  " ++ show err
          Right d -> do
            putMVar digraphM $ Just (d, Tar.entryTarPath tEntry)
      _ -> return ()
  digraphsInTar digraphM solved tRest

digraphDot digraphM fl = do
  dStr <- readFile fl
  case D.loadDot AM.emptyDigraph dStr of
    Left err -> putStrLn $ "Error parsing " ++ fl ++ ":\n  " ++ err
    Right (d , _, _, _, _) -> do
      putMVar digraphM $ Just (d, fl)

solve d dname = do
  t0 <- (D.numVertices d :: Int) `seq` getTime ProcessCPUTime
  let (_, cpNumber) = CP.optimal d
  t1 <- cpNumber `seq` getTime ProcessCPUTime
  let (_, fasNumber) = FS.minimumI d
  t2 <- fasNumber `seq` getTime ProcessCPUTime
  return Result{ rsName = dname
               , rsVertices = D.numVertices d
               , rsArcs = D.numArcs d
               , rsFAS = fasNumber
               , rsFASSeconds = fromIntegral $ (toNanoSecs $ diffTimeSpec t2 t1) `div` (10 :: Integer)^(9 :: Int)
               , rsCP = cpNumber
               , rsCPSeconds = fromIntegral $ (toNanoSecs $ diffTimeSpec t1 t0) `div` (10 :: Integer)^(9 :: Int)
               }
  
