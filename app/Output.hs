module Output where

import qualified HGraph.Directed as D
import qualified HGraph.Directed.Output as D
import qualified HGraph.Directed.Packing.Cycles.ArcDisjoint as CP
import qualified HGraph.Directed.EditDistance.Acyclic.ArcDeletion as FS
import qualified Data.Map as M

basicDotStyle :: D.DotStyle a
basicDotStyle = D.defaultDotStyle{ D.everyNode = [("shape", "circle")]}

colors :: [String]
colors = 
  [ "#1AC013"
  , "#FF5A5A"
  , "#5A73FF"
  , "#C8C924"
  , "#80CDFF"
  , "#9143ff"
  , "#FF5AFF"
  , "#f0922b"
  , "#ba6610"
  , "#00eda3"
  , "#FF5A5A"
  , "#0d6a0c"
  , "#1B2B8C"
  , "#0C6A67"
  , "#c41670"
  , "#510C6a"
  , "#8dff26"
  , "#99E599"
  ]
  ++
  colors

writeToFile :: ( D.Mutable t
               , D.Adjacency t
               , D.DirectedGraph t
               ) => FilePath
                 -> t Int
                 -> IO ()
writeToFile destination d = do
  let packing = fst $ CP.optimal d
      (hittingSet, _) = FS.minimumI d
      dotStyle = markCycles packing $ markHittingSet hittingSet basicDotStyle
      dot = D.toDot d dotStyle
  writeFile destination dot

markHittingSet :: Ord a => [(a, a)] -> D.DotStyle a -> D.DotStyle a
markHittingSet hittingSet dotStyle = 
  dotStyle
    { D.edgeAttributes = 
        foldr (\(e, a) m -> M.insertWith (++) e a m)
              (D.edgeAttributes dotStyle)
              ( map (\e -> (e, [("style", "dashed")])) hittingSet
              )
    }

markCycles :: Ord a => [[a]] -> D.DotStyle a -> D.DotStyle a
markCycles packing dotStyle  =
  dotStyle
    { D.edgeAttributes = 
        foldr (\(e, a) m -> M.insertWith (++) e a m)
              (D.edgeAttributes dotStyle)
              (concatMap 
                (\(cycle, color) -> 
                    map (\e -> (e, [("color", color)]))
                        ((last cycle, head cycle) : zip cycle (tail cycle)))
                (zip packing colors))
    }
