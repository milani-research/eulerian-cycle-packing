module Main where

import qualified HGraph.Directed as D
import qualified HGraph.Directed.Isomorphism as D
import qualified HGraph.Directed.Generator as D
import qualified HGraph.Directed.AdjacencyMap as AM
import qualified HGraph.Directed.Load as D
import qualified HGraph.Directed.Packing.Cycles.ArcDisjoint as CP
import qualified HGraph.Directed.EditDistance.Acyclic.ArcDeletion as FS
import qualified HSParallel.ProducerConsumer as P
import qualified Data.Map as M
import qualified Data.Array.IArray as A
import Control.Monad.Trans.State
import Control.Monad
import Control.Concurrent.MVar
import Data.List
import Data.Maybe
import System.Random
import System.Directory
import System.FilePath
import System.IO
import System.Random.Shuffle

import Output

main :: IO ()
main = do
  generateByArcSplitting

data Fingerprint = 
  Fingerprint
    { fVertices      :: Int
    , fHittingArcSet :: Int
    , fCyclePacking  :: Int
    }
  deriving (Eq, Show, Ord)

fingerprint :: ( AM.DirectedGraph t
               , AM.Adjacency t
               , AM.Mutable t)
               => t Int 
               -> Fingerprint
fingerprint d = 
  Fingerprint
    { fVertices = D.numVertices d
    , fHittingArcSet = snd $ FS.minimumI d
    , fCyclePacking = snd $ CP.optimal d
    }

generateByArcSplitting :: IO ()
generateByArcSplitting = do
  let dir = "results/arc-splitting"
      maxN = 20 :: Int
      maxDigraphs = 200 :: Int
  createDirectoryIfMissing True dir
  db <- loadDigraphs dir
  mCount <- newMVar $ sum $ map length $ M.elems db
  mDigraphs <- newMVar db
  _ <- forM [3..maxN] $ \n -> do
    let d = D.bidirectedCycle AM.emptyDigraph n
    addIfNew mDigraphs (d, Fingerprint{fVertices = n, fHittingArcSet = n, fCyclePacking = n})
  _ <- P.produceConsumeCount (generate mDigraphs maxN) (storeDigraph dir) maxDigraphs mCount
  return ()

loadDigraphs :: FilePath -> IO (M.Map Fingerprint [AM.Digraph Int])
loadDigraphs dir = do
  files <- getDirectoryContents dir
  digraphs <- forM files $ \fl -> do
    isFile <- doesFileExist fl
    if isFile then do
      withFile fl  ReadMode $ \h -> do
        str <- hGetContents h
        case D.loadDot AM.emptyDigraph str of
          Left err -> do
            hPutStrLn stderr $ fl ++ ":" ++ show err
            return Nothing
          Right (d, _, _ ,_, _) -> return $ Just d
    else
      return Nothing
  return $ foldr (\d m -> M.insertWith (++) (fingerprint d) [d] m) M.empty $ map fromJust $ filter isJust digraphs

generate :: ( AM.Adjacency t2
            , AM.Mutable t2
            , AM.DirectedGraph t2
            )
         => MVar (M.Map Fingerprint [t2 Int])
         -> Int
         -> IO (t2 Int, Fingerprint)
generate mDigraphs maxVertices = do
  db <- readMVar mDigraphs
  gen <- newStdGen
  let d = flip evalState gen $ do
        let loop = do
                   (fprint, dBases) <- randomNumber 0 (M.size db - 1) >>= \i -> return $ M.elemAt i db
                   if fVertices fprint >= (maxVertices - 2) then
                      loop
                   else do
                      let n = length dBases
                      randomNumber 0 (n - 1) >>= \i -> return (dBases !! i)
        dBase <- loop
        addCycle maxVertices dBase 
  let (_, hs) = FS.minimumI d
      (_, packing) = CP.optimal d
      fprint = Fingerprint{fVertices = D.numVertices d, fHittingArcSet = hs, fCyclePacking = packing}
  index <- addIfNew mDigraphs (d, fprint)
  case index of
    Just _ -> return (d, fprint)
    Nothing -> generate mDigraphs maxVertices

addIfNew :: ( AM.DirectedGraph t2
            , AM.Adjacency t2
            , Integral a
            , A.Ix a
            , Ord k)
         => MVar (M.Map k [t2 a])
         -> (t2 a, k)
         -> IO (Maybe Int)
addIfNew mDigraphs result = addIfNew' mDigraphs 0 result

addIfNew' :: ( AM.DirectedGraph t2
             , AM.Adjacency t2
             , Integral a
             , A.Ix a
             , Ord k)
          => MVar (M.Map k [t2 a])
          -> Int
          -> (t2 a, k)
          -> IO (Maybe Int)
addIfNew' mDigraphs verified result@(d, fprint) = do
  db <- readMVar mDigraphs
  case M.lookup fprint db of
    Nothing -> do
      added <- addNew mDigraphs 0 result
      if added then
        return $ Just 0
      else
        addIfNew' mDigraphs 0 result
    Just ds -> do
      let n = length ds
          ds' = take (n - verified) ds
      if and $ map (\h -> not $ d `D.isIsomorphicTo` h) ds' then do
        added <- addNew mDigraphs n result
        if added then
          return $ Just n
        else
          addIfNew' mDigraphs verified result
      else
        return Nothing

addNew :: Ord k
       => MVar (M.Map k [a])
       -> Int
       -> (a, k)
       -> IO Bool
addNew mDigraphs verified (d, fprint) = do
  db <- takeMVar mDigraphs
  case M.lookup fprint db of
    Nothing -> do
      putMVar mDigraphs $ M.insertWith (++) fprint [d] db
      return True
    Just ds -> 
      if length ds == verified then do
        putMVar mDigraphs $ M.insertWith (++) fprint [d] db
        return True
      else do
        putMVar mDigraphs db
        return False

storeDigraph :: ( AM.Mutable t
                , AM.Adjacency t
                , AM.DirectedGraph t
                , Show p)
             => [Char]
             -> (t Int, Fingerprint)
             -> p
             -> IO ()
storeDigraph dir (d, fprint) i = do
  let fname = intercalate "_" 
        [ "n=" ++ (show $ (D.numVertices d :: Int))
        , "hs=" ++ (show $ fHittingArcSet fprint) 
        , "p=" ++ (show $ fCyclePacking fprint) 
        , show i
        ]
      destination = (dir </> fname <.> "gv")
      wrongDegree = filter (\v -> (D.indegree d v :: Int) /= D.outdegree d v || D.indegree d v > (2 :: Int) || D.outdegree d v > (2 :: Int)) $ D.vertices d
  if not $ null wrongDegree then
    putStrLn $ "Error at " ++ destination ++ "\n\tvertex with wrong degree: " ++ show wrongDegree
  else
    return ()
  writeToFile destination d
  putStrLn destination

addCycle :: (AM.Adjacency t, AM.Mutable t, AM.DirectedGraph t)
         => Int
         -> t Int
         -> State StdGen (t Int)
addCycle maxVertices d = do
  let nMax = maxVertices - (D.numVertices d)
  n <- randomNumber 2 nMax
  m <- randomNumber 1 (min n (D.numArcs d))
  arcs' <- randomSubset (A.array (0, (D.numArcs d - 1)) $ zip [0..] (D.arcs d) :: (A.Array Int (Int, Int))) m
  subdivisions <- randomNumberPartition n m
  let (d', newVertices) = subdivideArcs d $ zip arcs' subdivisions
  addCycle' d' newVertices

addCycle' :: (D.DirectedGraph t, D.Mutable t, D.Adjacency t) => t Int -> [Int] -> State StdGen (t Int)
addCycle' d newVertices = do
  let n = (length newVertices) - 1
  is <- forM [0 .. n-1] $ \i -> randomNumber 0 (n - i)
  let path = shuffle newVertices is
  return $ foldr 
    (\(v,u) h ->
      if D.arcExists h (v,u) then
        let w = D.numVertices h
        in foldr D.addArc (D.addVertex w h) [(v,w), (w,u)]
      else
        D.addArc (v,u) h
    )
    d
    ((last path, head path) : (zip path $ tail path))

-- | Random non-empty, ordered number partition
randomNumberPartition :: (Monad m, RandomGen a2)
                      => Int
                      -> Int
                      -> StateT a2 m [Int]
randomNumberPartition number parts = 
  randomNumberPartition' (number - parts) 
                         parts 
                         (A.array (0, parts - 1) $ zip [0..parts - 1] (repeat 1) :: (A.Array Int Int))

randomNumberPartition' :: ( Monad m
                          , A.IArray a1 e
                          , A.Ix i
                          , RandomGen a2
                          , UniformRange i
                          , Num t
                          , Num i
                          , Num e
                          , Eq t)
                       => t
                       -> i
                       -> a1 i e 
                       -> StateT a2 m [e]
randomNumberPartition' 0 _ partition' = return $ A.elems partition'
-- | Random maybe empty, ordered number partition
randomNumberPartition' number parts partition' = do
  p <- randomNumber 0 (parts - 1)
  randomNumberPartition' (number - 1) parts (partition' A.// [(p, 1 + partition' A.! p)])

subdivideArcs :: (AM.Mutable t
                 , AM.DirectedGraph t
                 , Integral a)
              => t a
              -> [((a, a), a)]
              -> (t a, [a])
subdivideArcs d arcs = subdivideArcs' [] d arcs

subdivideArcs' :: (AM.Mutable t, AM.DirectedGraph t, Integral a)
               => [a]
               -> t a
               -> [((a, a), a)]
               -> (t a, [a])
subdivideArcs' newVertices d [] = (d, newVertices)
subdivideArcs' newVertices d (((v,u), n) : arcs) = 
  let ws = [D.numVertices d .. D.numVertices d + n - 1]
  in
  subdivideArcs' 
    (ws ++ newVertices) 
    (foldr D.addArc (foldr D.addVertex (D.removeArc (v,u) d) ws) 
      ( (v, head ws) : 
        (D.numVertices d + n - 1, u) : 
        (zip ws $ tail ws))
    )
    arcs

randomSubset :: (Num t
                , A.IArray a1 a2
                , A.Ix t
                , Monad m
                , RandomGen a3
                , UniformRange t
                )
            => a1 t a2
            -> t
            -> StateT a3 m [a2]
randomSubset xs k 
  | k > (snd $ A.bounds xs) - (fst $ A.bounds xs) + 1 = return []
  | otherwise = randomSubset' (fst $ A.bounds xs) (snd $ A.bounds xs) xs k

randomSubset' :: (Monad m
                 , RandomGen g
                 , UniformRange a
                 , Num a
                 , Num b
                 , Eq b
                 , A.IArray c1 c2
                 , A.Ix a
                 )
                 => a
                 -> a
                 -> c1 a c2
                 -> b
                 -> StateT g m [c2]
randomSubset' _ _ _ 0 = return []
randomSubset' iMin iMax xs k = do
  i <- randomNumber iMin iMax
  ys <- randomSubset' iMin (iMax - 1) (xs A.// [(i, xs A.! iMax)]) (k - 1)
  return $ (xs A.! i) : ys

randomNumber :: (Monad m, RandomGen a, UniformRange b)
            => b -> b -> StateT a m b
randomNumber n0 n1 = do
  g <- get
  let (n, g') = uniformR (n0, n1) g
  put g'
  return n
