

module Main where

import Output
import qualified HGraph.Directed as D
import qualified HGraph.Directed.AdjacencyMap as AM
import qualified HGraph.Directed.Load as D
import qualified HGraph.Directed.Output as D
import qualified HGraph.Directed.Packing.Cycles.ArcDisjoint as CP
import qualified HGraph.Directed.EditDistance.Acyclic.ArcDeletion as FS
import qualified Data.Map as M
import Control.Monad
import System.IO
import System.Environment
import System.Exit
import System.Directory
import System.FilePath

main = do
  args <- getArgs
  case args of
    (flIn : flOut : params) -> do
      md <- withFile flIn ReadMode $ \h -> do
        str <- hGetContents h
        case D.loadDot AM.emptyDigraph str of
          Left err -> do
            hPutStrLn stderr $ flIn ++ ":\n\t" ++ show err
            return Nothing
          Right result -> return $ Just result
      case md of
        Just (d, ltoi, itol, nodeAttr, edgeAttr) -> do
          let nodeAttr' = foldr (\v m -> M.insertWith (++) v [("label", itol M.! v)] m)
                                (M.mapKeys (ltoi M.!) $ M.map (map (\(var, val) -> (show var, show val))) nodeAttr) 
                                $ D.vertices d
              (nodeAttr'', edgeAttr'') = annotate params d nodeAttr' M.empty
              dotStyle = basicDotStyle
                { D.nodeAttributes = nodeAttr''
                , D.edgeAttributes = edgeAttr''
                }
          createDirectoryIfMissing True (takeDirectory flOut)
          writeFile flOut (D.toDot d dotStyle)
        Nothing -> do
          putStrLn $ "Failed to read DOT file " ++ flIn
          exitWith $ ExitFailure 2
    [] -> do
      putStrLn "Please provide an input .gv file and a destination file, in this order."
      exitWith $ ExitFailure 1

annotate ("feedback-arc-set" : params) d nodeAttr edgeAttr = 
  let (hs, _) = FS.minimumI d
  in annotate params d
        nodeAttr
        (foldr (\e m -> M.insertWith (++) e [("style", "dashed")] m) edgeAttr hs)
annotate ("cycle-packing" : params) d nodeAttr edgeAttr = 
  let packing = zip (fst (CP.optimal d)) colors
  in annotate params d
        nodeAttr
        (foldr (\(e, att) m -> M.insertWith (++) e att m) edgeAttr $
          (concatMap 
                (\(cycle, color) -> 
                    map (\e -> (e, [("color", color)]))
                        ((last cycle, head cycle) : zip cycle (tail cycle)))
                packing))
annotate [] d nodeAttr edgeAttr = (nodeAttr, edgeAttr)
