module Main where

import qualified HGraph.Directed as D
import qualified HGraph.Directed.Output as D
import qualified HGraph.Directed.Load as D
import qualified HGraph.Directed.AdjacencyMap as AM
import qualified HGraph.Directed.Generator.Eulerian as Eulerian
import Control.Concurrent
import Control.Monad
import Data.Either
import Data.Maybe
import System.Directory
import System.Environment
import System.Exit
import System.IO
import System.FilePath
import Text.Read

main :: IO ()
main = do
  args <- getArgs
  case args of
    [nStr, destination] ->
      let nE = readEither nStr
      in 
      case nE of
        Left err -> do
          putStrLn err
        Right n -> do
          createDirectoryIfMissing True destination
          existing <- listDirectory destination
          digraphsE <- fmap (map fromJust . filter isJust) $ forM existing $ \path -> do
            let fullPath = destination </> path
            isFile <- doesFileExist fullPath
            if isFile && (takeExtension path `elem` [".gv", ".dot"]) then do
              withFile fullPath ReadMode $ \h -> do
                dStr <- hGetContents h
                let dEither = D.loadDot AM.emptyDigraph dStr
                dEither `seq` (return $ Just (fullPath, dEither))
            else
              return Nothing
          let errors = filter (isLeft . snd) digraphsE
          if not $ null errors then do
            forM_ errors $ \error' -> case error' of
              (path, Left err) -> do
                putStrLn $ "Error when reading " ++ path ++ ":\n\t" ++ show err
              _ -> return ()
            exitWith $ ExitFailure 1
          else do
            let digraphs = map (\(d,_,_,_,_) -> fst $ D.linearizeVertices d) $ rights $ map snd digraphsE
                nDigraphs = length digraphs
            when (nDigraphs > 0) $ do
              putStrLn $ "Loaded " ++ show nDigraphs ++ " digraphs."
            let stateFl = destination </> "completed"
            minN <- do
              fileExists <- doesFileExist stateFl
              if fileExists then do
                stateStr <- readFile stateFl
                case readEither stateStr of
                  Right s -> return $ (s + 1 :: Int)
                  Left err -> do
                    putStrLn $ "Error while reading " ++ stateFl ++ ":\n\t" ++ err
                    return 1
              else
                return 1
            resultM <- newEmptyMVar
            let loop currentN = do
                       result <- takeMVar resultM
                       case result of
                         Nothing -> do
                            writeFile stateFl $ show currentN
                            putStrLn $ "Done. Output written to " ++ destination
                         Just (d,i) -> do
                            writeFile 
                              (destination </> (show (nDigraphs + i) ++ "_" ++ "n=" ++ show (D.numVertices d :: Int)) <.> "gv")
                              (D.toDot d D.defaultDotStyle)
                            if D.numVertices d > currentN then do
                              let n' = (D.numVertices d) + 1
                              writeFile stateFl (show n')
                              loop n'
                            else
                              loop currentN

            putStrLn $ "Starting from digraphs with " ++ show minN ++ " vertices."
            _ <- forkIO $ loop (minN - 1)
            Eulerian.enumerateParallel AM.emptyDigraph minN n 2 2 digraphs resultM (Just 30)
    _ -> do
      putStrLn "Please provide the maximum number of vertices desired and the output directory."
